namespace Codern.Recruitment.Core;

public class DailySummary
{
    public DateTime Date { get; set; }
    public int NumberOfMeasurements { get; set; }
    public double AverageTemperature { get; set; }
}