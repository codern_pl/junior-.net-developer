﻿using System;
using System.Collections.Generic;
using System.Linq;
using Codern.Recruitment.Core.Services;
using Codern.Recruitment.Dal.Entities;
using Xunit;

namespace Codern.Recruitment.Tests;

// ReSharper disable once InconsistentNaming
public class TemperatureCalculator_CalculateDailySummaries_Tests
{
    private readonly ITemperatureCalculator _calculator = new TemperatureCalculator();
    
    [Fact]
    public void Should_Return_Empty_Collection_When_Parameter_Collection_Is_Empty()
    {
        // Act
        var result = _calculator.CalculateDailySummariesFrom(new List<ThermometerReading>()).ToList();

        // Assert
        Assert.Empty(result);
    }
    
    [Fact]
    public void Should_Throw_ArgumentNullException_When_Collection_Is_Null()
    {
        // Act
        var act = () =>
        {
            _calculator.CalculateDailySummariesFrom(null);
        };

        // Assert
        Assert.Throws<ArgumentNullException>("readings" , act);
    }
    
    
    [Fact]
    public void Should_Calculate_Daily_Summaries_For_Unique_Dates()
    {
        // Arrange
        var readings = new List<ThermometerReading>
        {
            new()
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now,
                TemperatureC = 1
            },
            new()
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now.AddMinutes(2),
                TemperatureC = 2
            },
            new()
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now.AddDays(1),
                TemperatureC = 3
            }
        };

        // Act
        var result = _calculator.CalculateDailySummariesFrom(readings).ToList();

        // Assert
        Assert.Equal(2, result.Count);
        
        Assert.Equal(1.5, result[0].AverageTemperature);
        Assert.Equal(2, result[0].NumberOfMeasurements);
        
        Assert.Equal(3, result[1].AverageTemperature);
        Assert.Equal(1, result[1].NumberOfMeasurements);
    }
}