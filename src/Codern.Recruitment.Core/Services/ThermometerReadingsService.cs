﻿using Codern.Recruitment.Core.Dtos;
using Codern.Recruitment.Dal.Repositories;

namespace Codern.Recruitment.Core.Services;

public class ThermometerReadingsService : IThermometerReadingsService
{
    private readonly IThermometerReadingRepository _thermometerReadingRepository;

    public ThermometerReadingsService(IThermometerReadingRepository thermometerReadingRepository)
    {
        _thermometerReadingRepository = thermometerReadingRepository;
    }

    public async Task<IEnumerable<ThermometerReadingDto>> GetReadingsAsync(DateTime @from, DateTime to, int pageNumber, int pageSize,
        CancellationToken cancellationToken)
    {
        var results = await _thermometerReadingRepository.GetReadingsAsync(from, to, pageNumber, pageSize, cancellationToken);

        return results.Select(x => new ThermometerReadingDto
        {
            Id = x.Id,
            CreatedAt = x.CreatedAt,
            TemperatureC = x.TemperatureC,
            TemperatureF = 32 + (int)(x.TemperatureC / 0.5556)
        });
    }
}