namespace Codern.Recruitment.Dal.Entities;

public class ThermometerReading
{
    public Guid Id { get; set; }
    public DateTime CreatedAt { get; set; }
    public double TemperatureC { get; set; }
}