﻿using Codern.Recruitment.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Codern.Recruitment.Dal;

public class ThermometersDbContext : DbContext
{
    public ThermometersDbContext(DbContextOptions<ThermometersDbContext> options) : base(options)
    {
    }

    internal DbSet<ThermometerReading> ThermometerReadings { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var creationTime = new DateTime(2022, 01, 01, 0, 0, 0);

        foreach (var index in Enumerable.Range(1, 1000))
        {
            modelBuilder.Entity<ThermometerReading>().HasData(
                new ThermometerReading
                {
                    Id = Guid.NewGuid(),
                    CreatedAt = creationTime,
                    TemperatureC = GetRandomDouble(-5.0, 10.0),
                }
            );

            creationTime = creationTime.AddMinutes(60);
        }
    }

    private double GetRandomDouble(double minimum, double maximum) => Random.Shared.NextDouble() * (maximum - minimum) + minimum;
}