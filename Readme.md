﻿# Welcome to Codern .NET Recruitment

## Domain

Project is about devices that send data to our API. Device can be thermometer or localisation sensor or wind meter,
water meter etc.

## Solution architecture

Project has been written using N-tier architecture (also called "Layered Architecture").

<img height="500" src="/assets/architecture-diagram.png" width="450"/>

## Code structure

Solution contains 4 projects (all written in .NET 6). Project uses Entity Framework Core with In-Memory Database
provider. Database Seed is performed on application startup.

| Project                  | Description |
|--------------------------| ----------- |
| Codern.Recruitment.Api   | REST api project |
| Codern.Recruitment.Core  | Business Logic project |
| Codern.Recruitment.Dal   | Data Access Layer project |
| Codern.Recruitment.Tests | Unit Tests project |

# Your tasks

1. Add new endpoint for the devices to save thermometer readings. Url should be at
   `POST /thermometer-readings`.

- When the request is success api should return `HTTP 200 OK` code and added resource in json response.
- Remember about implementing incoming data validation. Reading has to be maximum 15 minutes old - if it is older api
  should return `HTTP 400 Bad Request`. Minimum temperature is should be `−273.15 °C`
  which is absolute zero (zero kelvin).

2. Add new endpoint to get reading by id `GET /thermometer-readings/{id}`.

- When the request is success api should return `HTTP 200 OK` code and added resource in json response.
- When there is no such reading in the db the response should be `HTTP 404 Not Found`

3. In tests project there is an interface `ITemperatureCalculator`. The implementation is missing and you should
   implement it. There are several unit tests for this interface. Read unit tests carefully first and then implement the
   behaviour to match expectations. After implementation your class should pass all tests. 


   

