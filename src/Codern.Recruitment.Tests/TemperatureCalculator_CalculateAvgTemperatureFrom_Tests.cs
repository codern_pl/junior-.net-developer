using System;
using System.Collections.Generic;
using System.Linq;
using Codern.Recruitment.Core.Services;
using Codern.Recruitment.Dal.Entities;
using Xunit;

namespace Codern.Recruitment.Tests;

// ReSharper disable once InconsistentNaming
public class TemperatureCalculator_CalculateAvgTemperatureFrom_Tests
{
    private readonly ITemperatureCalculator _calculator = new TemperatureCalculator();

    [Fact]
    public void Should_Return_Zero_When_Collection_Is_Empty()
    {
        // Act
        var result = _calculator.CalculateAvgTemperatureFrom(new List<ThermometerReading>());

        // Assert
        Assert.Equal(0, result);
    }
    
    [Fact]
    public void Should_Throw_ArgumentNullException_When_Collection_Is_Null()
    {
        // Act
        var act = () =>
        {
            _calculator.CalculateAvgTemperatureFrom(null);
        };

        // Assert
        Assert.Throws<ArgumentNullException>("readings" , act);
    }
    
    [Fact]
    public void Should_Calculate_Avg_Correctly_For_Negative_Values()
    {
        // Arrange
        var readings = new List<ThermometerReading>
        {
            new ThermometerReading
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now,
                TemperatureC = -1
            },
            new ThermometerReading
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now,
                TemperatureC = -2
            },
            new ThermometerReading
            {
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now,
                TemperatureC = -3
            }
        };

        // Act
        var result = _calculator.CalculateAvgTemperatureFrom(readings);

        // Assert
        Assert.Equal(-2, result);
    }
}