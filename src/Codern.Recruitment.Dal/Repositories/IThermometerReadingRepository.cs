﻿using Codern.Recruitment.Dal.Entities;

namespace Codern.Recruitment.Dal.Repositories;

public interface IThermometerReadingRepository
{
    Task<IEnumerable<ThermometerReading>> GetReadingsAsync(DateTime from, DateTime to, int pageNumber, int pageSize, CancellationToken cancellationToken);
}