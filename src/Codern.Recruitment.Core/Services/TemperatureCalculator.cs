using Codern.Recruitment.Dal.Entities;

namespace Codern.Recruitment.Core.Services;

public class TemperatureCalculator : ITemperatureCalculator
{
    public double CalculateAvgTemperatureFrom(IEnumerable<ThermometerReading> readings)
    {
        throw new NotImplementedException();
    }

    public IEnumerable<DailySummary> CalculateDailySummariesFrom(IEnumerable<ThermometerReading> readings)
    {
        throw new NotImplementedException();
    }
}