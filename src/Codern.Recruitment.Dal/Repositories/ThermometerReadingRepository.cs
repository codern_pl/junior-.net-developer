﻿using Codern.Recruitment.Dal.Entities;
using Microsoft.EntityFrameworkCore;

namespace Codern.Recruitment.Dal.Repositories;

internal class ThermometerReadingRepository : IThermometerReadingRepository
{
    private readonly ThermometersDbContext _dbContext;

    public ThermometerReadingRepository(ThermometersDbContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task<IEnumerable<ThermometerReading>> GetReadingsAsync(DateTime @from, DateTime to, int pageNumber, int pageSize, CancellationToken cancellationToken)
    {
        var query = _dbContext.ThermometerReadings
            .Where(x => x.CreatedAt >= from)
            .Where(x => x.CreatedAt <= to);

        if (pageNumber == 1)
        {
            return await query.Skip(0).Take(pageSize).ToListAsync(cancellationToken);
        }

        return await query
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync(cancellationToken);
    }
}