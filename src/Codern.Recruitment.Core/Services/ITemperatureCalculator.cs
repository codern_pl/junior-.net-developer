﻿using Codern.Recruitment.Dal.Entities;

namespace Codern.Recruitment.Core.Services;

public interface ITemperatureCalculator
{
    public double CalculateAvgTemperatureFrom(IEnumerable<ThermometerReading> readings);
    public IEnumerable<DailySummary> CalculateDailySummariesFrom(IEnumerable<ThermometerReading> readings);
}