﻿namespace Codern.Recruitment.Core.Dtos;

public class ThermometerReadingDto
{
    public Guid Id { get; set; }
    public DateTime CreatedAt { get; set; }
    public double TemperatureC { get; set; }
    public double TemperatureF { get; set; }

}