﻿using Codern.Recruitment.Core.Dtos;

namespace Codern.Recruitment.Core.Services;

public interface IThermometerReadingsService
{
    Task<IEnumerable<ThermometerReadingDto>> GetReadingsAsync(DateTime @from, DateTime to, int pageNumber, int pageSize,
        CancellationToken cancellationToken);
}