using Codern.Recruitment.Core.Dtos;
using Codern.Recruitment.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Codern.Recruitment.Api.Controllers;

/// <inheritdoc />
[ApiController]
[Route("thermometer-readings")]
public class ThermometerReadingsController : ControllerBase
{
    private readonly IThermometerReadingsService _thermometerReadingsService;

    /// <inheritdoc />
    public ThermometerReadingsController(IThermometerReadingsService thermometerReadingsService)
    {
        _thermometerReadingsService = thermometerReadingsService;
    }
    
    /// <summary>
    /// Returns paginated thermometer readings
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <param name="from"> From filter. Date format: 2022-01-01T00:00:00.000Z</param>
    /// <param name="to"> To filter. Date format: 2022-01-01T00:00:00.000Z</param>
    /// <param name="pageNumber">Page number filter</param>
    /// <param name="pageSize">Page size filter</param>
    [HttpGet(Name = "GetThermometerReadingsPage")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IEnumerable<ThermometerReadingDto>> Get(CancellationToken cancellationToken, DateTime from, DateTime to, int pageNumber = 1, int pageSize = 10)
    {
        return await _thermometerReadingsService.GetReadingsAsync(from, to, pageNumber, pageSize, cancellationToken);
    }
}